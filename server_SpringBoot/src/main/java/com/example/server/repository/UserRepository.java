package com.example.server.repository;

import com.example.server.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {

    @Query(
            value = "SELECT * FROM USERS u WHERE u.username = :username",
            nativeQuery = true)
    public User findUserByUserName(@Param("username") String username);
}
