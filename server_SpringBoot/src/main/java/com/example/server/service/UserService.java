package com.example.server.service;

import com.example.server.model.User;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserService {

    public void saveUser(User user);

    public List<User> getAllUsers();

    User findUserByUserName(String username);

  /*  Optional<User> findByEmail(String email);
    List<User> findByFirstName(String name);
    void deleteByEmail(String email);*/
}
