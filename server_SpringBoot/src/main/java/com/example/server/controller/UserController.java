package com.example.server.controller;

import com.example.server.model.User;
import com.example.server.repository.UserRepository;
import com.example.server.service.UserService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = {"http://localhost:3000"})
@RestController
@RequestMapping("/api/user")
public class UserController {


    @Autowired
    UserService userService;


    @GetMapping("/getAll")
    public List<User> testSearch() {
        List<User> users = userService.getAllUsers();
        return users;
    }


    @PostMapping("/save")
    public String save(@RequestBody User user) {
        userService.saveUser(user);
        return "New user is created";
    }

    //todo  сделать на форме, чтобы нельзя отправлять пустые данные.
    //CHECK USER IF EXISTS
    @PostMapping("/checklogin")
    public LogResulte checklogin(@RequestBody User user) {
        LogResulte logResulte = new LogResulte("Заглушка. доделать фронт", false);
        User userDB = userService.findUserByUserName(user.getUsername());
        if (userDB != null && userDB.getUsername() != null) {
            if (user.getPassword() == userDB.getPassword()) {
                logResulte.setSrt("Success");
                logResulte.setCorrect(true);
                return logResulte;
            } else {
                logResulte.setSrt("Неправильно введён логин");
                logResulte.setCorrect(false);
                return logResulte;
            }
        } else {
            logResulte.setSrt("Пользователь не найден");
            logResulte.setCorrect(false);
            return logResulte;
        }
    }
}