import { useContext } from "react";
import "./stories.scss"
import { AuthContext } from "../../context/authContext"

const Stories = () => {

    const {currentUser} = useContext(AuthContext)

    //MOCK DATA
    const stories = [
        {
            id: 1,
            name: "Test User",
            img: "https://images.unsplash.com/photo-1658027355985-70e60a0e4ac5?auto=format&fit=crop&q=80&w=1946&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",        },
        {
            id: 2,
            name: "Test User",
            img: "https://images.unsplash.com/photo-1658027355985-70e60a0e4ac5?auto=format&fit=crop&q=80&w=1946&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",        },
        {
            id: 3,
            name: "Test User",
            img: "https://images.unsplash.com/photo-1658027355985-70e60a0e4ac5?auto=format&fit=crop&q=80&w=1946&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",        },
        {
            id: 4,
            name: "Test User",
            img: "https://images.unsplash.com/photo-1658027355985-70e60a0e4ac5?auto=format&fit=crop&q=80&w=1946&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
        },
    ];

    return (
        <div className="stories">
            <div className="story">
                <img src={currentUser.profilePic} alt="" />
                <span>{currentUser.name}</span>
                <button>+</button>
            </div>
            {stories.map(story=>
            <div className="story" map={story.id}>
                <img src={story.img} alt=""/>
                <span>{story.name}</span>
            </div>
            )}
        </div>
    )
}

export default Stories
