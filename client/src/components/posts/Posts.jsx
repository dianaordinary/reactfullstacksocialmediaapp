import Post from "../post/Post";
import "./posts.scss";

const Posts = () => {
    //MOCK DATA
    const posts = [
        {
            id: 1,
            name: "One User",
            userId: 1,
            profilePic:
                "https://images.unsplash.com/photo-1699024571491-f7088816e8ba?auto=format&fit=crop&q=80&w=1935&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
            desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit",
            img: "https://images.unsplash.com/photo-1688057934204-1537ae77028c?auto=format&fit=crop&q=80&w=1974&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
        },
        {
            id: 2,
            name: "Second User",
            userId: 2,
            profilePic:
                "https://images.unsplash.com/photo-1415226194219-638f50c5d25f?auto=format&fit=crop&q=80&w=1974&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",
            desc: "Tenetur iste voluptates dolorem rem commodi voluptate pariatur, voluptatum, laboriosam consequatur enim nostrum cumque! Maiores a nam non adipisci minima modi tempore.",
        },
    ];

    return <div className="posts">
        {posts.map(post =>(
               <Post post={post} key={post.id}/>
            ))
        }
    </div>;
};

export default Posts;
