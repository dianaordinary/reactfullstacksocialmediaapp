import './App.scss';
import {
    createBrowserRouter,
    RouterProvider,
    Route,
    Outlet,
    Navigate,
} from "react-router-dom";
import "./style.scss";

import Login from "./pages/login/Login";
import Register from "./pages/register/Register";
import Navbar from "./components/navbar/Navbar";
import Home from "./pages/home/Home";
import {useContext} from "react";
import {DarkModeContext} from "./context/darkModeContext";
import BarLeft from "./components/barLeft/BarLeft";
import BarRight from "./components/barRight/BarRight";
import {AuthContext} from "./context/authContext";


function App() {

    let {currentUser} = useContext(AuthContext);

    const { darkMode } = useContext(DarkModeContext);

    const Layout = () => {
        return (
            <div className={`theme-${darkMode ? "dark" : "light"}`}>
                <Navbar />
                <div style={{ display: "flex" }}>
                    <BarLeft />
                    <div style={{ flex: 6 }}>
                        <Outlet />
                    </div>
                    <BarRight />
                </div>
            </div>
        );
    };

    const router = createBrowserRouter([
        {
            path: "/",
            element: <Layout/>,
            children: [
                {
                    path: "/",
                    element: <Home/>,
                },
                {
                    /* path: "/profile/:id",
                     element: <Profile />,*/
                },
            ],
        },

        {
            path: "/login",
            element: <Login/>,
        },
        {
            path: "/register",
            element: <Register/>,
        },
    ]);


    return (
        /*<div className="App">
          <header className="App-header">*/
        <RouterProvider router={router}/>
        /*</header>
      </div>*/
    );
}

export default App;
