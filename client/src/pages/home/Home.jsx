import "./home.scss"
import {useState, useEffect} from 'react';
import Stories from "../../components/stories/Stories";
import Posts from "../../components/posts/Posts";
import Profile from "../profile/Profile";

const Home = () => {

    return (
        <div className="home">
            {/*<Stories/>
            <Posts/>*/}

            <Profile/>
        </div>
    );
}

export default Home
